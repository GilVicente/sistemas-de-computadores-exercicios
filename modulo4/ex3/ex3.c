/* Includes */
#include <unistd.h>     /* Symbolic Constants */
#include <sys/types.h>  /* Primitive System Data Types */ 
#include <errno.h>      /* Errors */
#include <stdio.h>      /* Input/Output */
#include <stdlib.h>     /* General Utilities */
#include <pthread.h>    /* POSIX Threads */
#include <string.h>     /* String handling */
#include <semaphore.h>  /* Semaphore */

#include <wait.h>
#include <sys/stat.h>
#include <fcntl.h>		// Para as variáveis O_CREAT, O_EXCL etc
#include <sys/mman.h>


int main(int argc, char *argv[]){
	
	FILE *fp;
	sem_t *semaforo;
	pid_t pid;
	//char *name = "semaforo";
	
	if ((semaforo = sem_open("semaforo", O_CREAT, 0644, 1)) == SEM_FAILED) {	// Abre semáforo
 		perror("No sem_open()");
 		exit(1);
	}
	
	sem_wait(semaforo);
	
	fp = fopen(argv[1], "a+");	// Abre e permite escrita no ficheiro
	fprintf(fp,"Eu sou o processo com o PID %d\n", pid = getpid());
	sleep(2);
	
	sem_post(semaforo);
	
	sem_unlink("semaforo");
	
	return 0;
	
}
