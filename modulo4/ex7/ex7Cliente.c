/* Includes */
#include <unistd.h>     /* Symbolic Constants */
#include <sys/types.h>  /* Primitive System Data Types */ 
#include <errno.h>      /* Errors */
#include <stdio.h>      /* Input/Output */
#include <stdlib.h>     /* General Utilities */
#include <pthread.h>    /* POSIX Threads */
#include <string.h>     /* String handling */
#include <semaphore.h>  /* Semaphore */

#include <wait.h>
#include <sys/stat.h>
#include <fcntl.h>		// Para as variáveis O_CREAT, O_EXCL etc
#include <sys/mman.h>
#include <time.h>

#define bilhetes_Max 1000	// Lotacao máxima
#define max_wait_time 10

typedef struct{
	int bilhetes[bilhetes_Max];
}atendedor;

int main(int argc, char *argv[]){
	int fd;
	int data_size = sizeof(atendedor)*bilhetes_Max;
	atendedor *shm_atendedor;
	pid_t pid;
	int bilhete_nr, i;
	time_t t;
	srand((unsigned) time(&t));

	int childs = atoi(argv[1]);
	printf("Childs %d\n", childs);
	sem_t *semaforos[childs];
	
	/* Abrir o objeto da memória partilhada */
    if ((fd = shm_open("/shm_ex07mod", O_CREAT|O_RDWR,S_IRUSR|S_IWUSR)) < 0) {
        /* Tratamento de erros em caso de falha da operacao anterior */
        fprintf(stderr, "ERROR: No shm_open()\n");
        exit(EXIT_FAILURE);
    }
    /* Ajustar o tamanho da memória partilhada */
    ftruncate(fd, data_size);
    /* Mapear a memória partilhada */
    shm_atendedor = (atendedor *) mmap(NULL, data_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
 
	
	for(i = 0; i < childs; i++){
		char sem_name[50];
		sprintf(sem_name,"semaforo%d",i);
		if ((semaforos[i] = sem_open(sem_name, O_CREAT, 0644, 0)) == SEM_FAILED) {	// Abre semáforos
 		perror("No sem_open()");
 		exit(1);
		}
	}
	
	sem_post(semaforos[0]);
	
	for(i = 0; i < childs; i++){
		pid = fork();
		if (pid == 0){
			sem_wait(semaforos[i]);						// Apenas passa neste wait o filho com o valor do i correspondente
			bilhete_nr = shm_atendedor->bilhetes[i];	// ao semaforo aberto. Desta forma a execucão dos filhos segue a 
			printf("Já fui atentido ! Bilhete número:%d \n", bilhete_nr); // sequência de criação
			
			sleep(rand() % max_wait_time);
			sem_post(semaforos[i+1]);
			exit(0);
		}
	}
	
	for(i = 0; i < childs; i++){
		char sem_name[50];
		sprintf(sem_name,"semaforo%d",i);				// Fecha os semaforos
		if((sem_unlink(sem_name) == -1)){
			perror("No sem_unlink()");
			exit(EXIT_FAILURE);
		}
	}
	
	// Desfaz mapeamento
	munmap(shm_atendedor, data_size);
	// Fecha o descritor devolvido pelo shm_open
	close(fd);
	// O Leitor apaga a memória Partilhada do Sistema 
	shm_unlink("/shm_ex07mod"); 
	
	exit(EXIT_SUCCESS);
}
