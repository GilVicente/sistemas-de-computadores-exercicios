/* Includes */
#include <unistd.h>     /* Symbolic Constants */
#include <sys/types.h>  /* Primitive System Data Types */ 
#include <errno.h>      /* Errors */
#include <stdio.h>      /* Input/Output */
#include <stdlib.h>     /* General Utilities */
#include <pthread.h>    /* POSIX Threads */
#include <string.h>     /* String handling */
#include <semaphore.h>  /* Semaphore */

#include <wait.h>
#include <sys/stat.h>
#include <fcntl.h>		// Para as variáveis O_CREAT, O_EXCL etc
#include <sys/mman.h>

#define bilhetes_Max 10	// Lotacao máxima

typedef struct{
	int bilhetes[bilhetes_Max];
}atendedor;

int main(void){
	
	int fd;
	int data_size = sizeof(atendedor) * bilhetes_Max;
	atendedor *shm_atendedor;
	int i;
		
	/* Abrir o objeto da memória partilhada */
    if ((fd = shm_open("/shm_ex07mod", O_CREAT|O_RDWR,S_IRUSR|S_IWUSR)) < 0) {
        /* Tratamento de erros em caso de falha da operacao anterior */
        fprintf(stderr, "ERROR: No shm_open()\n");
        exit(EXIT_FAILURE);
    }
    /* Ajustar o tamanho da memória partilhada */
    ftruncate(fd, data_size);
    /* Mapear a memória partilhada */
    shm_atendedor = (atendedor *) mmap(NULL, data_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);

    
	for(i = 0; i < bilhetes_Max; i++){
		shm_atendedor->bilhetes[i] = i+1;			// Preenche o número de cada bilhete
	}
	exit(EXIT_SUCCESS);
}
