/* Includes */
#include <unistd.h>     /* Symbolic Constants */
#include <sys/types.h>  /* Primitive System Data Types */ 
#include <errno.h>      /* Errors */
#include <stdio.h>      /* Input/Output */
#include <stdlib.h>     /* General Utilities */
#include <pthread.h>    /* POSIX Threads */
#include <string.h>     /* String handling */
#include <semaphore.h>  /* Semaphore */

#include <wait.h>
#include <sys/stat.h>
#include <fcntl.h>		// Para as variáveis O_CREAT, O_EXCL etc
#include <sys/mman.h>

int main(void){
	
	FILE *fp;
	sem_t *semaforo;
	pid_t pid;
	int i,j;
	char buff[1000];
	
	fp = fopen("file.txt", "w");	// Abre e permite escrita no ficheiro
	
	if ((semaforo = sem_open("semaforo", O_CREAT, 0644, 1)) == SEM_FAILED) {	// Abre semáforo
 		perror("No sem_open()");
 		exit(1);
	}
	//printf("ASDADABB \n");
	//fprintf(fp,"fileee \n");
	for(i = 0; i < 5; i++){
		pid = fork();
		
		if(pid == 0){
			sem_wait(semaforo);
			fprintf(fp,"%dº filho !\n\n",i+1);
			for(j = i*20 ; j < i*20+20; j++){
				fprintf(fp, "%d\n",j+1);
			}
			sem_post(semaforo);
			exit(0);
		}
	}
	fclose(fp);	// Closes the write
	if(pid > 0){
		for(i = 0; i < 5; i++){		// Espera pelos 5 filhos
			wait(NULL);
		}
		
		fp = fopen("file.txt", "r");	// Opens the write
		while(fgets(buff,1000, fp) != NULL){
			printf("%s", buff);
		}
	
		fclose(fp);
	}
	
	return 0;
}
