/* Includes */
#include <unistd.h>     /* Symbolic Constants */
#include <sys/types.h>  /* Primitive System Data Types */ 
#include <errno.h>      /* Errors */
#include <stdio.h>      /* Input/Output */
#include <stdlib.h>     /* General Utilities */
#include <pthread.h>    /* POSIX Threads */
#include <string.h>     /* String handling */
#include <semaphore.h>  /* Semaphore */

#include <wait.h>
#include <sys/stat.h>
#include <fcntl.h>		// Para as variáveis O_CREAT, O_EXCL etc
#include <sys/mman.h>
#include <time.h>

#define length 100

typedef struct {
	char string_matrix[4][3][length]; // matriz com 4 linhas, 3 colunas e strlen 100
}matrix;

int leituraM(matrix *shm_data); // retorna indice da mensagem retirada, -1 se nao retirar nenhuma

int main(int argc, char *argv[]) {
	
	sem_t *sem_empty, *sem_excl, *sem_row[4];
	
	int data_size = sizeof(matrix);
    matrix *shm_data; /* apontador para a memória partilhada */
    int fd, n,i;
 
    
    /* abrir o objeto de memória partilhada (sem o criar) */
    fd = shm_open("/ex13", O_RDWR, S_IRWXU | S_IRWXG);
    if (fd < 0) {
        perror("ERROR shm_open()");
        exit(1);
    }
    
    /* ajustar o tamanho da mem. partilhada */
    ftruncate(fd, data_size);
    
    /* mapear a mem. partilhada */
    shm_data = (matrix *)mmap(NULL, data_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    if (shm_data == NULL) {
        perror("ERROR mmap()");
        exit(1);
    }
    
    /* criar semaforos da fila*/ 
    for(i = 0; i < 4; i++){
		char sem_file[8];
		sprintf(sem_file, "%s%d","/sem_row12", i);
		if((sem_row[i] = sem_open(sem_file,0)) == SEM_FAILED) {			// abrir semaforos
			perror("ERROR sem_open()");
			exit(1);
		}
	}
    /* abrir o semáforo exclusao mutua */
    if ((sem_excl = sem_open("/sem_excl12",0)) == SEM_FAILED) {
        perror("ERROR sem_open()");
        exit(1);
    }
	/* abrir semáforo para verificar se as filas estao vazias */
    if ((sem_empty = sem_open("/sem_empty12", 0)) == SEM_FAILED) {
        perror("ERROR sem_open()");
        exit(1);
    }
    
    sem_wait(sem_excl);					// garante exclusao mutua
    n = leituraM(shm_data);
    sem_post(sem_empty);				// incrementa semaforo 
    sem_post(sem_row[n]);				// incrementa semaforo da fila correspondente
	sem_post(sem_excl);
	
	for(i = 0; i<4;i++){
	
		if(sem_close(sem_row[i]) == -1){
			perror("ERROR sem_close()");
			exit(1);
		}
	
	}
	
	if(sem_close(sem_excl) == -1){
		perror("ERROR sem_close()");
		exit(1);
	}
	if(sem_close(sem_empty) == -1){
		perror("ERROR sem_close()");
		exit(1);
	}
	
	if(close(fd) == -1){
		perror("ERROR close()");
		exit(1);
	}
	
    return 0;
}

int leituraM(matrix *shm_data){
	int i,j;
	for(i = 0; i < 4; i++){
		for(j = 0; j < 3; j++){		
			if(shm_data->string_matrix[i][j] != 0 && shm_data->string_matrix[i][j] != NULL){
				printf("%s\n", shm_data->string_matrix[i][j]);
				strcpy(shm_data->string_matrix[i][j],"\0");
				return i;
			}
		}
	}
	return -1;
}
