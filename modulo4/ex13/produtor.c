/* Includes */
#include <unistd.h>     /* Symbolic Constants */
#include <sys/types.h>  /* Primitive System Data Types */ 
#include <errno.h>      /* Errors */
#include <stdio.h>      /* Input/Output */
#include <stdlib.h>     /* General Utilities */
#include <pthread.h>    /* POSIX Threads */
#include <string.h>     /* String handling */
#include <semaphore.h>  /* Semaphore */

#include <wait.h>
#include <sys/stat.h>
#include <fcntl.h>		// Para as variáveis O_CREAT, O_EXCL etc
#include <sys/mman.h>
#include <time.h>
#define length 100

typedef struct {
	char string_matrix[4][3][length]; // matriz com 4 linhas, 3 colunas e strlen 100
}matrix;

int verifica(int priority, matrix *shm_data); // esta funcao retorna o indice se houver espaco ou -1 se estiver cheio

void escreveM(int priority, matrix *shm_data, int index);

int main(int argc, char *argv[]) {
	
	sem_t *sem_empty, *sem_excl, *sem_row[4];
	
	int data_size = sizeof(matrix);
    matrix *shm_data;
    int i, fd, index;
    int n = -1;
	
    /* criar o objeto de memoria partilhada */
	fd = shm_open("/ex13", O_CREAT | O_RDWR, S_IRWXU | S_IRWXG);
    if (fd < 0) {
        perror("No shm_open()");
        exit(1);
    }
    /* ajustar o tamanho da mem. partilhada */
    ftruncate(fd, data_size);

	/* mapear a mem. partilhada */
	shm_data = (matrix *)mmap(NULL, data_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    if (shm_data == NULL) {
        perror("ERROR mmap()");
        exit(1);
    }
    
    /* CRIAÇAO DE SEMAFOROS de fila*/ 
    for(i = 0; i < 4; i++){
		char sem_file[8];
		sprintf(sem_file, "%s%d","/sem_row12", i);
		if((sem_row[i] = sem_open(sem_file, O_CREAT|O_EXCL, 0664, 3)) == SEM_FAILED) {	// criar semaforos
			sem_row[i] = sem_open(sem_file,0);
		}
	}
    
    /* criar semáforo para exclusao mutua com valor = 1 */
    if ((sem_excl = sem_open("/sem_excl12", O_CREAT|O_EXCL, 0664, 1)) == SEM_FAILED) {
        perror("ERROR sem_open()");
        exit(1);
    }
   
    /* criar semáforo para verificar se esta vazio com valor = 0 */
    if ((sem_empty = sem_open("/sem_empty12", O_CREAT|O_EXCL , 0664, 0)) == SEM_FAILED) {
        perror("ERROR sem_open()");
        exit(1);
    }
    
    
    sem_wait(sem_excl);					// garante exclusao mutua
										// so um processo pode ter acesso à memoria partilhada
    
    while(n < 0 || n > 3){
		printf("Prioridade da mensagem(0-3): ");
		scanf("%d", &n);
	}
	sleep(5);
	sem_wait(sem_row[n]);				// se ficar bloqueado quer dizer que a fila está cheia
	index = verifica(n, shm_data);
	if(index == -1){
		printf("Erro");
		exit(1);
	}
	printf("Posição disponivel: %d\n", index);
	escreveM(n, shm_data, index);
	sem_post(sem_empty);
	
	
	sem_post(sem_excl);
        
    
    return 0;
}

int verifica(int priority, matrix *shm_data){
	int i;
	for(i = 0; i < 3; i++){
		if(strlen(shm_data->string_matrix[priority][i]) == 0 || shm_data->string_matrix[priority][i] == NULL){
			return i;
		}
	}
	return -1;
}
void escreveM(int priority, matrix *shm_data, int index){
	char msg[length];
	printf("Escreva a sua mensagem: ");
	scanf("%s", msg);
	
	strcpy(shm_data->string_matrix[priority][index],msg);
}
