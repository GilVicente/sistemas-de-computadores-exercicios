/* Includes */
#include <unistd.h>     /* Symbolic Constants */
#include <sys/types.h>  /* Primitive System Data Types */ 
#include <errno.h>      /* Errors */
#include <stdio.h>      /* Input/Output */
#include <stdlib.h>     /* General Utilities */
#include <pthread.h>    /* POSIX Threads */
#include <string.h>     /* String handling */
#include <semaphore.h>  /* Semaphore */

#include <wait.h>
#include <sys/stat.h>
#include <fcntl.h>		// Para as variáveis O_CREAT, O_EXCL etc
#include <sys/mman.h>
#include <time.h>

int main(void){
	int fd; 																// descritor
	int *array;																// array para os 10 inteiros
	int data_size = sizeof(int)*10;
	sem_t *semaforoWrite1,*semaforoWrite2, *semaforoRead;
	pid_t pid;
	int i,j;
	time_t t;
	srand((unsigned) time(&t));
											
    if ((fd = shm_open("/shm_ex09", O_CREAT|O_RDWR,S_IRUSR|S_IWUSR)) < 0) {	// Abrir o objeto da memória partilhada
        fprintf(stderr, "ERROR: No shm_open()\n");
        exit(EXIT_FAILURE);
    }																		
    ftruncate(fd, data_size);												// Ajustar o tamanho da memória partilhada																	
    array = (int *) mmap(NULL, data_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);	// Mapear a memória partilhada
    
    if ((semaforoWrite1 = sem_open("semaforoW1", O_CREAT, 0644, 1)) == SEM_FAILED) {	// Abre semáforos
 		perror("No sem_open()");
 		exit(1);
	}
	if ((semaforoWrite2 = sem_open("semaforoW2", O_CREAT, 0644, 0)) == SEM_FAILED) {	// Abre semáforos
 		perror("No sem_open()");
 		exit(1);
	}
	
	if ((semaforoRead = sem_open("semaforoR", O_CREAT, 0644, 0)) == SEM_FAILED) {	// Abre semáforos
 		perror("No sem_open()");
 		exit(1);
	}

	
	for(i = 0; i < 2; i++)
	{
		pid = fork();
		if(pid == 0)
		{
			srand(time(NULL) - i*2);										// Garante que os números aleatorios sao diferentes
																			// para cada filho
			printf("INSIDE\n");
			if(i == 0)
			{	
				sem_wait(semaforoWrite1);										// Exclusão mútua do semaforo
				for(j = 0; j < 10; j++)											
				{	
					array[j] = rand()%10;										// Produtores escrevem
					printf("Array[%d]: %d \n",j,array[j]);
				}
				sem_post(semaforoRead);											// Vai para leitura
				
				sem_wait(semaforoWrite1);										// Espera pela leitura
				
				for(j = 0; j < 5; j++)
				{
					array[j] = rand()%10;										// Produtores escrevem
					printf("Array[%d]: %d \n",j,array[j]);
				}
				sem_post(semaforoWrite2);										// Passa a escrita ao filho 2
			}
			else if(i == 1)
			{	
				sem_wait(semaforoWrite2);										// Escreve array[5 ao 9]
				for(j = 5; j < 10; j++)											
				{	
					array[j] = rand()%10;										// Produtores escrevem
					printf("Array[%d]: %d \n",j,array[j]);
				}
				sem_post(semaforoRead);											// Vai para leitura
				
				sem_wait(semaforoWrite2);										// Espera para escrever pela 3a vez
				
				for(j = 0; j < 10; j++)
				{
					array[j] = rand()%10;										// Produtores escrevem
					printf("Array[%d]: %d \n",j,array[j]);
				}
				sem_post(semaforoRead);											// Vai para leitura
			}
			printf("OUT\n");
			exit(0);
		}
	}
	
	// Waits for read-time
	sem_wait(semaforoRead);
	for(i = 0; i < 3; i++)					// Lê 30 valores, 3*10 valores
	{
		printf("Inside reader: \n");
		for(j = 0; j < 10; j++)
		{
			printf("%d iteracao: %d \n", i+1, array[j]);
		}
		if(i == 0)
		{
			sem_post(semaforoWrite1);
			sem_wait(semaforoRead);	
		}
		if(i == 1)
		{
			sem_post(semaforoWrite2);
			sem_wait(semaforoRead);
		}
	}
	if((sem_unlink("semaforoW1") == -1)){
		perror("No sem_unlink(1)");
		exit(EXIT_FAILURE);
	}
	
	if((sem_unlink("semaforoW2") == -1)){
		perror("No sem_unlink(2)");
		exit(EXIT_FAILURE);
	}
	
	if((sem_unlink("semaforoR") == -1)){
		perror("No sem_unlink(3)");
		exit(EXIT_FAILURE);
	}
	
	// Desfaz mapeamento
	munmap(array, data_size);
	// Fecha o descritor devolvido pelo shm_open
	close(fd);
	// O Leitor apaga a memória Partilhada do Sistema 
	shm_unlink("/shm_ex09"); 
    
	return 0;
}
