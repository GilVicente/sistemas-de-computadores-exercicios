/* Includes */
#include <unistd.h>     /* Symbolic Constants */
#include <sys/types.h>  /* Primitive System Data Types */ 
#include <errno.h>      /* Errors */
#include <stdio.h>      /* Input/Output */
#include <stdlib.h>     /* General Utilities */
#include <pthread.h>    /* POSIX Threads */
#include <string.h>     /* String handling */
#include <semaphore.h>  /* Semaphore */

#include <wait.h>
#include <sys/stat.h>
#include <fcntl.h>		// Para as variáveis O_CREAT, O_EXCL etc
#include <sys/mman.h>
#include <time.h>

typedef struct {
	pid_t pid;
	time_t mytime;
	int nrEscritores;
	int nrLeitores;
} estrutura;

int main(){
	int fd;
	estrutura *shm_estrutura;
	sem_t *semContaEscritores, *semContaLeitores, *semExclusaoEscritores, *semControlo;
    
    
    if ((semContaEscritores = sem_open("semContaEscritores", O_CREAT, 0644, 1)) == SEM_FAILED) {	// Abre semáforos
 		perror("No sem_open()");
 		exit(1);}
	if ((semContaLeitores = sem_open("semContaLeitores", O_CREAT, 0644, 1)) == SEM_FAILED) {	// Abre semáforos
 		perror("No sem_open()");
 		exit(1);}
 	if ((semExclusaoEscritores = sem_open("semExclusaoEscritores", O_CREAT, 0644, 1)) == SEM_FAILED) {	// Abre semáforos
 		perror("No sem_open()");
 		exit(1);}
	if ((semControlo = sem_open("semControlo", O_CREAT, 0644, 1)) == SEM_FAILED) {	// Abre semáforos
 		perror("No sem_open()");
 		exit(1);}

	
    /******************MEMORIA*************/
    //Cria e abre uma área de memória partilhada	
	fd = shm_open("/shm_ex10", O_CREAT|O_RDWR, S_IRUSR|S_IWUSR);
		if (fd<0){
			perror("ERROR: No shm_open()\n");
			exit(EXIT_FAILURE);	
		}
	//Definir que a área de memória partilhada tem data_size bytes de tamanho
	ftruncate (fd, sizeof(estrutura));
	
	//Mapear a memória partilhada
	shm_estrutura = (estrutura*)mmap(NULL,sizeof(estrutura),PROT_READ|PROT_WRITE,MAP_SHARED,fd,0);

    /**************************************/
    
    
    /************Action************************/
    //controla quantos filhos estao a ser leitores
    sem_wait(semControlo); // faz com que os escritores esperem pelo fim do leitor
    
    sem_wait(semContaLeitores);
    shm_estrutura->nrLeitores++;
    sem_post(semContaLeitores);
    
    // faz a leitura da memoria
    printf("Leitor :\n");    
    printf("Pid lido: %d,Tempo atual lido:\n",shm_estrutura->pid);
    printf(ctime(&shm_estrutura->mytime));
    printf("Numero de leitores no momento:%d\n\n\n",shm_estrutura->nrLeitores); 
    shm_estrutura->nrLeitores--;
    
    
    sem_post(semControlo);		//liberta para o escritor
	exit(EXIT_SUCCESS);
}
