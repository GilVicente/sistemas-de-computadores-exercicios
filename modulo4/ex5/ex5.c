/* Includes */
#include <unistd.h>     /* Symbolic Constants */
#include <sys/types.h>  /* Primitive System Data Types */ 
#include <errno.h>      /* Errors */
#include <stdio.h>      /* Input/Output */
#include <stdlib.h>     /* General Utilities */
#include <pthread.h>    /* POSIX Threads */
#include <string.h>     /* String handling */
#include <semaphore.h>  /* Semaphore */

#include <wait.h>
#include <sys/stat.h>
#include <fcntl.h>		// Para as variáveis O_CREAT, O_EXCL etc
#include <sys/mman.h>

int main(void){
	
	pid_t pid;
	sem_t *semaforo;
	
	if ((semaforo = sem_open("semaforo", O_CREAT, 0644, 0)) == SEM_FAILED) {	// Abre semáforo
 		perror("No sem_open()");
 		exit(1);
	}
	
	if ((pid = fork()) == -1){
		perror("Fork failed.\n");
		exit(EXIT_FAILURE);
	}
	
	if(pid > 0){
		printf("Eu sou o pai ! \n");
		sem_post(semaforo);
	}	
	if(pid == 0){
		sem_wait(semaforo);
		printf("Eu sou o filho ! \n");
	}
	
	exit(EXIT_SUCCESS);
	
}
