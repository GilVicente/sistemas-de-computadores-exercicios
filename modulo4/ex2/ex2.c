/* Includes */
#include <unistd.h>     /* Symbolic Constants */
#include <sys/types.h>  /* Primitive System Data Types */ 
#include <errno.h>      /* Errors */
#include <stdio.h>      /* Input/Output */
#include <stdlib.h>     /* General Utilities */
#include <pthread.h>    /* POSIX Threads */
#include <string.h>     /* String handling */
#include <semaphore.h>  /* Semaphore */

#include <wait.h>
#include <sys/stat.h>
#include <fcntl.h>		// Para as variáveis O_CREAT, O_EXCL etc
#include <sys/mman.h>

int main(void){
	
	FILE *fp;
	sem_t *semaforo0;
	sem_t *semaforo1;
	sem_t *semaforo2;
	sem_t *semaforo3;
	sem_t *semaforo4;
	pid_t pid;
	int i,j;
	char buff[1000];
	
	semaforo0 = sem_open("semaforo0", O_CREAT, 0644, 1);
	semaforo1 = sem_open("semaforo1", O_CREAT, 0644, 0);
	semaforo2 = sem_open("semaforo2", O_CREAT, 0644, 0);
	semaforo3 = sem_open("semaforo3", O_CREAT, 0644, 0);
	semaforo4 = sem_open("semaforo4", O_CREAT, 0644, 0);
	
	sem_t *sems[5] = {semaforo0, semaforo1, semaforo2, semaforo3, semaforo4};

	fp = fopen("file.txt", "w");	// Abre e permite escrita no ficheiro
	for(i = 0; i < 5; i++){
		if ((pid = fork()) == -1){
			perror("Fork failed.\n");
			exit(EXIT_FAILURE);
		}
		if(pid == 0){
			// Espera que o Semáforo tenha um valor superior a 0 para executar. E decrementa o seu valor
			sem_wait(sems[i]);
			fprintf(fp,"%dº filho !\n\n",i+1);
			for(j = i*2 ; j < i*2+2; j++)
			{
				fprintf(fp, "%d\n",j+1);
			}
			if(i < 4){
				sem_post(sems[i+1]);
			}
			exit(EXIT_SUCCESS);
		}
	}
	fclose(fp);	// Closes the write
	if(pid > 0){
		for(i = 0; i < 5; i++){		// Espera pelos 5 filhos
			wait(NULL);
		}
		fp = fopen("file.txt", "r");	// Opens the write
		while(fgets(buff,1000, fp) != NULL){
			printf("%s", buff);
		}
		fclose(fp);	
		if((sem_unlink("semaforo0") == -1)){
			perror("No sem_unlink()");
			exit(EXIT_FAILURE);
		}
		if((sem_unlink("semaforo1") == -1)){
			perror("No sem_unlink()");
			exit(EXIT_FAILURE);
		}
		if((sem_unlink("semaforo2") == -1)){
			perror("No sem_unlink()");
			exit(EXIT_FAILURE);
		}
		if((sem_unlink("semaforo3") == -1)){
			perror("No sem_unlink()");
			exit(EXIT_FAILURE);
		}
		if((sem_unlink("semaforo4") == -1)){
			perror("No sem_unlink()");
			exit(EXIT_FAILURE);
		}
	}
	return 0;
}
