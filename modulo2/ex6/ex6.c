#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>


#define PROCESSES_TO_CREATE 5

int main(void) {
	int fd[PROCESSES_TO_CREATE][2];
	int size_arrays = 1000; 
	int *vec1 = (int *) malloc(size_arrays * sizeof(int)); 
	int *vec2 = (int *) malloc(size_arrays * sizeof(int)); 
	pid_t pid;
	int i;

	//	Inicializar Arrays
	for (i = 0; i < size_arrays; ++i)
	{
		vec1[i] = i;
		vec2[i] = i;
	}

	//	Criar um Pipe para cada comunicacao processo-pai e processo-filho
	for (i = 0; i < PROCESSES_TO_CREATE; ++i)
	{
		// Criar o pipe entre o processo-pai e um processo-filho
		if(pipe(fd[i]) == -1) {
			perror("ERROR: Pipe failed.\n");
			return 1;
		} 

		pid = fork();

		// Se for um processo-filho
		if(pid == 0) {
			break; 
		}
	}
	// Para o filho
	if (pid == 0) {
		// Fechar extermidade nao usada - read
		close(fd[i][0]);

		// Calcula soma Parcial de 200 posicoes
		int j, sum = 0;
		int lim = (i+1) * 200;
		for (j = i*200; j < lim; ++j)
		{
			sum = (vec1[j] + vec2[j]);
			/* Escrita no Pipe da soma dos valores do vector */
			write(fd[i][1], &sum, sizeof(int));
		}

		// Fechar extremidade usada - write 
		close(fd[i][1]);
	}


	// Para o pai
	if (pid > 0) {
		int status;
		int *result = (int *) malloc(size_arrays * sizeof(int)); // Array de Resultados

		// Aguardar pelo fim dos filhos
		for (i = 0; i < PROCESSES_TO_CREATE; ++i)
		{
			wait(&status);
		}
		int val = 0;
		int j;
		int lim;
		// Leitura dos valores somados pelos filhos
		for (i = 0; i < PROCESSES_TO_CREATE; ++i)
		{
			// Fechar extremidade nao usada - write
			close(fd[i][1]);
			// Leitura das 200 somas do filho actual
			lim = 200 * (i+1);
			for (j = i*200; j < lim; ++j)
			{
				read(fd[i][0], &val, sizeof(int));
				result[j] += val;
			}
			
			// Fechar extremidade usada - read
			close(fd[i][0]);
		}

	}

	return 0;
}
