#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <string.h>
 
 
 
int main ( void ){
   
    int read_msg;
    int write_msg;
   
    int fd[2];
    pid_t pid;
   
    /*Cria o pipe*/
    if(pipe(fd) == -1){
        perror("Pipe failed");
        return 1;  
    }
   
   
    pid = fork();
 
   
    if(pid > 0){
       
        //fecha a extremidade não usada
        close(fd[0]);
                   
        printf("PID do processo PAI -> %d\n",getpid());
       
        //guarda o pid do pai na variavel write_msg
        write_msg= (int) getpid();
        //escrever no pipe
        write(fd[1], &write_msg,sizeof(int));
        //fecha a extremidade de escrita.
        close(fd[1]);
    }
    else{
        //fecha o extremidade nao usada.
        close(fd[1]);
       
        //le dados do pipe
        read(fd[0], &read_msg, sizeof(int));
        printf("\nFilho leu %d\n", read_msg);
       
        //fecha o extremidade de leitura.
        close(fd[0]);
    }
   
   
    return 0;
 }
