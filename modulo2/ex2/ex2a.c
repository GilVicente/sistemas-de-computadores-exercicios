#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <string.h>


int main(){
	
	int numero;
	char nome[20];
	pid_t pid;
	int fd[2];
	
	/*Cria o pipe*/
    if(pipe(fd) == -1){
        perror("Pipe failed");
        return 1;  
    }
	
	pid = fork();
	
	if( pid > 0){
		printf("Número: ");
		scanf("%d", &numero);
		
		printf("Nome: ");
		scanf("%s", nome);
		
		//fecha a extremidade não usada -- fecha a leitura
		close(fd[0]);
		
		write(fd[1], &numero, sizeof(int));
		
		write(fd[1], nome, strlen(nome)+1);
		
		close(fd[1]);
	}
	else if (pid == 0){
		
		int nr;
		char name[20];
		//fecha o extremidade nao usada.
        close(fd[1]);
       
        //le dados do pipe
        read(fd[0], &nr, sizeof(int));
        printf("\nFilho leu %d\n", nr);
        
        //le dados do pipe
        read(fd[0], name, strlen(nome)+1);
        printf("\nFilho leu %s\n", name);
       
        //fecha o extremidade de leitura.
        close(fd[0]);
	}
	
	
	return 0;
}
