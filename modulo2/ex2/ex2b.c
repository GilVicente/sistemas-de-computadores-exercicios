#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <string.h>
#include <string.h>

#define STR_SIZE 20

typedef struct{
	char name[STR_SIZE];
	int number;
}dados;

int main(){
	pid_t pid;
	int fd[2];
	
	/*Cria o pipe*/
    if(pipe(fd) == -1){
        perror("Pipe failed");
        return 1;  
    }
    
    pid = fork();
    
    if( pid > 0 ){
		
		dados dataWrite;
		
		printf("Nome: ");
		scanf("%s", dataWrite.name);
		
		printf("Numero: ");
		scanf("%d", &dataWrite.number);
		
		close(fd[0]);
		
		write(fd[1], &dataWrite, sizeof(dados));
		
		close(fd[1]);
		
	}
	else if( pid == 0){
		dados dataRead;
		
		close(fd[1]);
		
		read(fd[0], &dataRead, sizeof(dados));
		
		printf("Name: %s \n", dataRead.name);
		printf("Numero: %d \n", dataRead.number);
		
		close(fd[0]);
	}
    
    
    
	return 0;
}
