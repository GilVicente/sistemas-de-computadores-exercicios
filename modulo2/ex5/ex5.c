#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>

#define PROCESSES_TO_CREATE 5

int main(void) {
	int fd[PROCESSES_TO_CREATE][2]; // Matriz de descritores de ficheiros dos Pipes
	int size_arrays = 1000; // Tamanho dos Arrays 
	int *vec1 = (int *) malloc(size_arrays * sizeof(int));
	int *vec2 = (int *) malloc(size_arrays * sizeof(int));
	pid_t pid;
	int i;

	/*	Inicializar Arrays com valores (sequenciais) */
	for (i = 0; i < size_arrays; ++i)
	{
		vec1[i] = i;
		vec2[i] = i;
	}

	//	Criar um Pipe para cada comunicaÃ§Ã£o processo-pai e processo-filho
	for (i = 0; i < PROCESSES_TO_CREATE; ++i)
	{
		// Criar o pipe entre o processo-pai e um processo-filho
		pipe(fd[i]);


		pid = fork(); // Cria um processo-filho

		// Se for um processo-filho
		if(pid == 0) {
			break; // Sai do Ciclo For
		}
	}
	
	// Se for um processo-filho
	if (pid == 0) {
		// Fechar extermidade nao usada - read
		close(fd[i][0]);

		// Calcula soma Parcial de 200 posicoes dos vectores
		int j, partial_sum = 0;
		int lim = (i+1) * 200;
		for (j = i*200; j < lim; ++j)
		{
			partial_sum += (vec1[j] + vec2[j]);
		}

		// Escrita no Pipe da soma parcial
		write(fd[i][1], &partial_sum, sizeof(int));

		// Fechar extremidade usada - write
		close(fd[i][1]);
	}


	// Se for o processo-pai
	if (pid > 0) {
		int status;
		int total_sum = 0, partial_sum = 0;

		//	Leitura de somas parciais dos filhos
		for (i = 0; i < PROCESSES_TO_CREATE; ++i)
		{
			wait(&status); // Espera que um processo-filho termine e limpa da tabela de processos

			// Fechar extremidade nao usada - write
			close(fd[i][1]);

			read(fd[i][0], &partial_sum, sizeof(int));
			total_sum += partial_sum;

			// Fechar extremidade usada - read 
			close(fd[i][0]);
		}

		printf("Total sum: %d\n", total_sum);
		
	}

	return 0;
}
