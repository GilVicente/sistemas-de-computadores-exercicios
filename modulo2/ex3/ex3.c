#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

#define STR_SIZE 20

int main(void) {
	int fd[2];
	pid_t pid;

	/* Criar o pipe */
	if(pipe(fd) == -1) {
		perror("ERROR: Pipe failed.\n");
		return 1;
	} 

	pid = fork(); /* Cria o processo-filho */

	if (pid > 0) {
		/* Se for o processo-pai */
		
		char msg1[STR_SIZE] = "Hello world!\0";
		char msg2[STR_SIZE] = "Goodbye!\0";

		//Fechar extermidade não usada
		close(fd[0]);

		
		// Escrita no Pipe da mensagem 1
		write(fd[1], &msg1, STR_SIZE);


		// Escrita no Pipe da mensagem 2
		write(fd[1], &msg2, STR_SIZE);

		// Fechar extremidade usada - write 
		close(fd[1]);


		int status;
		wait(&status); // Espera que o processo-filho termine e limpa da tabela de processos

	} 
	else if (pid == 0) {
		// Se for o processo-filho
		int i;
		char msg_read[STR_SIZE];

		// Fechar extremidade nao usada
		close(fd[1]);

		for (i = 0; i < 2; ++i)
		{
			// Leitura via Pipe da mensagem numero i
			read(fd[0], &msg_read, STR_SIZE);

			printf("Message Nr. %d: %s\n", i+1, msg_read);
		}

		// Fechar extremidade usada - read 
		close(fd[0]);
	}

	return 0;
}
