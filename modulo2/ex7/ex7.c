#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>

#define CHILD_PROCESSES 10
#define SIZE_MESSAGE 5

// Estrutura que representa uma jogada
typedef struct {
	char message[SIZE_MESSAGE];
	int round_nr;
} play;

int main(void) {
	int fd[2];
	pid_t pid;
	int i;

	// Cria um pipe entre o processo-pai e os processos-filhos
	if(pipe(fd) == -1) {
		perror("ERROR: Pipe failed.\n");
		return 1;
	} 

	for (i = 0; i < CHILD_PROCESSES; ++i)
	{
		pid = fork(); // Cria processo filho

		// Se for filho
		if(pid == 0) {
			break;
		}
	}
	
	// Se for filho
	if (pid == 0) {
		play p_read;
		// Fechar extremidade nao usada - write
		close(fd[1]);

		// Leitura do Pipe da estrutura enviada (com mensagem e numero de ronda)
		read(fd[0], &p_read, sizeof(p_read));

		printf("%s - PID = %d\n", p_read.message, getpid()); //	Apresenta Mensagem e respectivo PID do processo filho
		
		// Fechar extermidade usada - read
		close(fd[0]);
		exit(p_read.round_nr); // Termina Processo com o numero da ronda em que ganhou
	}


	// Se for o pai
	if (pid > 0) {
		play p; // inicializa a estrutura
		strcpy(p.message, "Win!\0"); // define a mensagem a passar na estrutura
		
		// Fechar extermidade nao usada - read
		close(fd[0]);	

		// Para 10 Rondas
		for (i = 0; i < 10; ++i) {
			
			p.round_nr = i+1; // define o numero da ronda na estrutura
			
			// Escreve a estrutura no pipe
			write(fd[1], &p, sizeof(p));

			sleep(2); // Adormece o pai durante 2 segundos
		}

		// Fechar extremidade usada - write
		close(fd[1]);

		printf("\n");

		int status;
		pid_t cpid;

		// Espera pelo fim dos filhos
		for (i = 0; i < 10; ++i) {
			cpid = wait(&status); // Espera pelo fim do filho e guarda o pid respectivo
			printf("Winner Round %d - Child-Process %d\n", WEXITSTATUS(status), cpid); // Apresenta Ronda Vencedora e o seu PID
		}
	}

	return 0;
}
