#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define CHILD_PROCESSES 10

// Estrutura que representa um registo de uma venda
typedef struct {
	int codigo_cliente;
	int codigo_produto;
	int quantidade;
} registo;

int main(void) {
	int fd[CHILD_PROCESSES][2];
	pid_t pid;
	int i, j;
	int count_expected = 0; // Contador de Valor Final Esperado

	registo *vendas = (registo *) malloc(50000 * sizeof(registo)); // Inicializacao do array de registos
	
	// Preenchimento do array vendas com valores aleatorios
	for (i = 0; i < 50000; ++i) {
		vendas[i].codigo_cliente = i;
		vendas[i].codigo_produto = i;
		if (i % 2 == 0) {
			vendas[i].quantidade = 21;
			count_expected++;
		} else {
			vendas[i].quantidade = 20;
		}
	}

	printf("Expected Registers: %d\n", count_expected); // imprime valor esperado de output

	// Cria os processos-filhos para processamento distribuido
	for (i = 0; i < CHILD_PROCESSES; ++i) {
		if(pipe(fd[i]) == -1) { 
			perror("ERROR: Pipe failed.\n");
			return 1;
		}	 
		
		pid = fork();

		if (pid == 0) {
			break;
		}
	}
	
	if (pid == 0) {
		int lim = 5000 * (i+1); // Calcula limite associado ao processo-filho actual

		// Fechar extermidade usada - read
		close(fd[i][0]);

		// Percorre 5000 registos associados ao processo actual
		for (j = 5000*i; j < lim; ++j) {
			// Se a quantidade do registo actual for superior a 20
			if (vendas[j].quantidade > 20) {
				// Escreve no Pipe a venda actual
				write(fd[i][1], &vendas[i], sizeof(registo));
			}
		}	
		
		// Fechar extremidade usada - write
		close(fd[i][1]);
	}


	// Se for o pai
	if (pid > 0) {
		int count = 0; 
		registo *vendas_maiores = (registo *) malloc(50000 * sizeof(registo)); // Alocacao de memoria para registo das vendas de qtd superior a 20

		int status;
		// Espera pelo fim dos processos-filho
		for (i = 0; i < CHILD_PROCESSES; ++i)
		{
			wait(&status);
			}
		}

		// Para os 10 Processos filho
		for (i = 0; i < CHILD_PROCESSES; ++i) {
			// Fechar extermidade nao usada - write
			close(fd[i][1]);
			while (read(fd[i][0], &vendas_maiores[count], sizeof(registo)) > 0) {
				count++; // Incrementa contador de registos lidos com qtd superior a 20
			}

			// Fechar extremidade usada - reading 
			close(fd[i][0]);
		}

		/* Reajusta vector criado para albergar as vendas com quantidades superiores a 20 para o espaÃ§o extritamente necessÃ¡rio */
		registo *temp = (registo *) realloc(vendas_maiores, (count-1) * sizeof(registo));
		
		// Se o reajustamento foi efectuado com sucesso
		if(temp != NULL){
			// Troca do apontador temporario para o efectivo 
   			vendas = temp;
   			temp = NULL;
		} else {
			// Apresentacao de mensagem de erro em caso de falha do realloc
			perror("ERROR: Failed to realloc.");
		}

		printf("Registers Counted: %d\n", count); // apresentacao de contagem de registos efectivamente lidos atraves dos pipes
		
	}

	return 0;
}
