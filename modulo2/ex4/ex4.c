#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>

int main(void) {
	int fd[2];
	pid_t pid;

	/* Criar o pipe */
	if(pipe(fd) == -1) {
		perror("ERROR: Pipe failed.\n");
		return 1;
	} 

	pid = fork(); // Cria o processo-filho

	if (pid > 0) {
		// Se for o processo-pai
		
		// Leitura do Ficheiro de Texto
		FILE *file_read = fopen("readme.txt", "rb");
		fseek(file_read, 0, SEEK_END);
		long file_size = ftell(file_read);
		fseek(file_read, 0, SEEK_SET);
		char *str_read = malloc(file_size + 1); 
		fread(str_read, file_size, 1, file_read); 
		file_size++; 
		fclose(file_read); 
		str_read[file_size] = '\0'; 

		// Fechar extermidade nao usada - read
		close(fd[0]);

		
		// Escrita no Pipe do tamanho do ficheiro lido
		write(fd[1], &file_size, sizeof(long));

		// Escrita no Pipe do conteudo do ficheiro
		write(fd[1], str_read, file_size);


		// Fechar extremidade usada - write
		close(fd[1]);

		int status;
		wait(&status); // Espera que o processo-filho termine e limpa da tabela de processos

	} 
	else if (pid == 0) {
		// Se for o processo-filho
		
		// Fechar extremidade nao usada - write
		close(fd[1]);


		long size = 0; // inicializacao do tamanho do ficheiro a receber
		
		// Leitura via pipe do tamanho do ficheiro lido
		read(fd[0], &size, sizeof(long));


		char *msg_read = (char *) malloc(size); // alocar memoria dinamica para ler o conteudo do ficheiro via pipe
		// Leitura via Pipe do conteudo do ficheiro 
		read(fd[0], msg_read, size);
	
		
		printf("File Content:\n- %s\n", msg_read); // imprime conteudo do ficheiro

		// Fechar extremidade usada - read
		close(fd[0]);

	}

	return 0;
}
