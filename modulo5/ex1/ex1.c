/* Includes */
#include <unistd.h>     /* Symbolic Constants */
#include <sys/types.h>  /* Primitive System Data Types */ 
#include <errno.h>      /* Errors */
#include <stdio.h>      /* Input/Output */
#include <stdlib.h>     /* General Utilities */
#include <pthread.h>    /* POSIX Threads */
#include <string.h>     /* String handling */

#include <sys/stat.h>
#include <fcntl.h>		// Para as variáveis O_CREAT, O_EXCL etc
#include <sys/mman.h>

void *identity_show(void *id);

typedef struct{
	int numero;
	char *nome;
	char *morada;
}identity;

int main(){
	
	identity v[5];
	v[0].numero = 1; v[1].numero = 2; v[2].numero = 3; v[3].numero = 4; v[4].numero = 5;
	v[0].nome = "A"; v[1].nome = "B"; v[2].nome = "C"; v[3].nome = "D"; v[4].nome = "E";
	v[0].morada = "MA";v[1].morada = "MB";v[2].morada = "MC";v[3].morada = "MD";v[4].morada = "ME";
	pthread_t threads[5];
	int i;

	for(i=0;i<5;i++)
		pthread_create(&threads[i], NULL,identity_show,(void*)&v[i]);
	printf("Todas as threads foram criadas\n");

	for(i=0;i<5;i++)
		pthread_join(threads[i], NULL);

	printf("Todas as threads terminaram\n");
	
	return EXIT_SUCCESS;
}

void *identity_show(void *id){
	identity *ident = (identity*) id;
	printf("Numero: %d ", ident->numero);
	printf("Nome:	%s ", ident->nome);
	printf("Morada: %s \n", ident->morada);
	pthread_exit((void*)NULL);
}


