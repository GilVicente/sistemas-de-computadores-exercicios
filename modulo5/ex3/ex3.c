/* Includes */
#include <unistd.h>     /* Symbolic Constants */
#include <sys/types.h>  /* Primitive System Data Types */ 
#include <errno.h>      /* Errors */
#include <stdio.h>      /* Input/Output */
#include <stdlib.h>     /* General Utilities */
#include <pthread.h>    /* POSIX Threads */
#include <string.h>     /* String handling */

#include <sys/stat.h>
#include <fcntl.h>		// Para as variáveis O_CREAT, O_EXCL etc
#include <sys/mman.h>

#define THREADS 5
#define ARRAY_SIZE 1000
#define RANGE 200

int saldos[ARRAY_SIZE];
int somaTotal;
pthread_mutex_t mux_totalSaldos;

void *soma_200(void *pos);

int main(){
	pthread_t thread[THREADS];
	int args[THREADS];
	int status;
	int i;
	
	// Cria Mutex para gerir acesso a variável global - Saldo Total Banco
	if ((status = pthread_mutex_init(&mux_totalSaldos, NULL))) {
		fprintf(stderr, "ERROR: No pthread_mutex_init() - error code: %d\n", status);
		exit(EXIT_FAILURE);
	}
	
	//Inicializa valores de saldos[ARRAY_SIZE]
	for(i = 0; i < ARRAY_SIZE; i++)
		saldos[i] = i;
	
	for(i=0;i<THREADS;i++)	//Cria as 5 threads
	{
		args[i] = i;
		pthread_create(&thread[i], NULL,soma_200,(void*)&args[i]);
	}
	printf("Todas as threads foram criadas\n");

	for(i=0;i<5;i++)
		pthread_join(thread[i], NULL);

	printf("Todas as threads terminaram\n");
	
	pthread_mutex_destroy(&mux_totalSaldos);
	
	printf("Saldo total dos clientes: %d\n", somaTotal);
	
	return EXIT_SUCCESS;
}

void *soma_200(void *pos){
	int *posInit = (void*) pos;
	
	*posInit = (*posInit)*RANGE;	// Posicao inicial
	int end = (*posInit)+RANGE;		// Posicao final
	
	
	int i,somaParcial = 0;
	for(i = *posInit; i < end; i++){	// Faz a soma parcial
		somaParcial += saldos[i];		
	}
	
	pthread_mutex_lock(&mux_totalSaldos); // Espera por acesso à var. Saldo Total Banco
	somaTotal += somaParcial; // Adiciona os 200 Saldos calculados ao saldo total do banco
	pthread_mutex_unlock(&mux_totalSaldos); // Dá acesso à var. Saldo Total Banco

	pthread_exit((void*)NULL);
}
