/* Includes */
#include <unistd.h>     /* Symbolic Constants */
#include <sys/types.h>  /* Primitive System Data Types */ 
#include <errno.h>      /* Errors */
#include <stdio.h>      /* Input/Output */
#include <stdlib.h>     /* General Utilities */
#include <pthread.h>    /* POSIX Threads */
#include <string.h>     /* String handling */


#define THREADS 5
#define SIZE 1000
#define RANGE 200

pthread_mutex_t mux;
pthread_cond_t cond[5]; 
int resultado[SIZE];
int dados[SIZE];

int currentPrinter = 0;

void *thread_func(void *pos);

int main(){
	int i;
	int args[THREADS];
	pthread_t thread[THREADS];

	for(i=0; i<THREADS; i++){				// Cria 5 variáveis de condição
		pthread_cond_init(&cond[i],NULL);
	}

	for(i=0; i<SIZE; i++){					// Inicializa vetor dados[]
		dados[i] = i;
	}
	for(i=0;i<THREADS;i++)	//Cria as 5 threads
	{
		args[i] = i;
		pthread_create(&thread[i], NULL,thread_func,(void*)&args[i]);
	}
	
	for(i=0;i<THREADS;i++)					// Espera pelos threads
		pthread_join(thread[i], NULL);
	
	return EXIT_SUCCESS;
}

void *thread_func(void *pos){
	int *posReceived = (int*) pos;
	int posInit = *posReceived * RANGE;
	int end = posInit + RANGE;

	int i;
	for(i = posInit; i < end; i++){
		resultado[i] = dados[i]*2+10;
	}
	
	pthread_mutex_lock(&mux);
	while(currentPrinter != *posReceived ){						
		pthread_cond_wait(&cond[*posReceived],&mux);	// Espera pela condicão
	}	
	
	for(i = posInit; i < end; i++){
		printf("Resultado: %d\n", resultado[i]);
	}
	
	if(*posReceived != THREADS - 1){
		pthread_cond_signal(&cond[*posReceived+1]);		// Sinaliza para a execucao da próxima thread
		currentPrinter++;								
	}

	pthread_mutex_unlock(&mux);
	
	pthread_exit((void*)NULL);
}
