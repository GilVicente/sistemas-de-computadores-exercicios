/* Includes */
#include <unistd.h>     /* Symbolic Constants */
#include <sys/types.h>  /* Primitive System Data Types */ 
#include <errno.h>      /* Errors */
#include <stdio.h>      /* Input/Output */
#include <stdlib.h>     /* General Utilities */
#include <pthread.h>    /* POSIX Threads */
#include <string.h>     /* String handling */

typedef struct{
	int nrAluno; 			//numero do Aluno
	int notaG1; 			//notas
	int notaG2;
	int notaG3;
	float notaFinal;
}Prova;

Prova vecAlunos[300];
int args[2];
int j=0,i=0;
float positivas = 0, negativas = 0;
pthread_mutex_t mux;
pthread_cond_t cond[300];
pthread_cond_t notaPositiva;
pthread_cond_t notaNegativa;
pthread_mutex_t muxPositiva;
pthread_mutex_t muxNegativa; 

void* preencheVetor(void* arg);
int geraNotaNum(int min, int max);
void* calculaNotaFinal(void* arg);
void printStruct();
void contaNotas(void *arg);

int main(void){
	pthread_t threads[3];
	int i;
	int arg1 = 0;
	int arg2 = 1;
	pthread_mutex_init(&mux,NULL);
	for(i=0; i<300; i++){				// Cria 300 variáveis de condição
		pthread_cond_init(&cond[i],NULL);
	}
		
	pthread_create(&threads[0], NULL, preencheVetor, NULL);
	pthread_create(&threads[1], NULL, calculaNotaFinal, NULL);
	pthread_create(&threads[2], NULL, calculaNotaFinal, NULL);
	pthread_create(&threads[3], NULL, contaNotas,(void*)&arg1 );
	pthread_create(&threads[4], NULL, contaNotas, (void*)&arg2);
	
	

	pthread_create(&threads[i], NULL, calculaNotaFinal, (void*)&args[i-1]);

	
	for(i=1;i<3;i++){
		pthread_join(threads[i],NULL);
	}
	
	//printStruct();
	
	printf("Negativas: %f\nPositivas: %f\n", negativas, positivas);
	
	printf("Percentagem Positivas: %.2f\nPercentagem Negativas: %.2f\n",positivas/300*100,negativas/300*100);
	
	return 0;
}

void preenche(){
	
	vecAlunos[i].nrAluno = i;
	vecAlunos[i].notaG1 = geraNotaNum(0,20);
	vecAlunos[i].notaG2 = geraNotaNum(0,20);
	vecAlunos[i].notaG3 = geraNotaNum(0,20);
}

void* preencheVetor(void* arg){
	
	int i;
	for(i=0; i<300; i++){
		preenche(&vecAlunos[i],i);
		pthread_cond_signal(&cond[i]);
	}
	
	pthread_exit(NULL);
}

void * calculaNotaFinal(void*arg){
	
	for(i=0;i<300;i++){	
			pthread_mutex_lock(&mux);
			pthread_cond_wait(&cond[i],&mux);
			vecAlunos[i].notaFinal = (float)(vecAlunos[i].notaG1+vecAlunos[i].notaG2+vecAlunos[i].notaG3)/3;
		
			if(vecAlunos[i].notaFinal >= 50){
				pthread_cond_signal(&notaPositiva);
			}
			else{
				pthread_cond_signal(&notaNegativa);
			}
			pthread_mutex_unlock(&mux);
	}

	pthread_exit(NULL);
}

void * contaNotas(void *arg){
	int valor = *((int *) arg);
	
	if(valor == 0){
		while(positivas + negativas < 300){
			pthread_mutex_lock(&muxNegativa);
			pthread_cond_wait(&notaNegativa,&muxNegativa);
			negativas++;
			pthread_mutex_unlock(&muxNegativa);
		}
	}
	
	if(valor == 1){
		while(positivas+negativas < 300){
			pthread_mutex_lock(&muxPositiva);
			pthread_cond_wait(&notaPositiva,&muxPositiva);
			negativas++;
			pthread_mutex_unlock(&muxPositiva);
		}
	}
	
	pthread_exit(NULL);
	
}

int geraNotaNum(int min,int max){
	srand((unsigned)time(NULL)+getpid()+getppid()*j);
    j++;
    
    return (rand() % (max+1-min))+min;
}

void printStruct(){
	int i;
	for(i=0;i<300;i++){
		printf("\nNumero Aluno: %d\n", vecAlunos[i].nrAluno);
		printf("Nota G1: %d\n", vecAlunos[i].notaG1);
		printf("Nota G2: %d\n", vecAlunos[i].notaG2);
		printf("Nota G3: %d\n", vecAlunos[i].notaG3);
		printf("Nota Final: %f\n", vecAlunos[i].notaFinal);
	}
}


