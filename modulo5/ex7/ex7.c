/* Includes */
#include <unistd.h>     /* Symbolic Constants */
#include <sys/types.h>  /* Primitive System Data Types */ 
#include <errno.h>      /* Errors */
#include <stdio.h>      /* Input/Output */
#include <stdlib.h>     /* General Utilities */
#include <pthread.h>    /* POSIX Threads */
#include <string.h>     /* String handling */

#define TOTALPRODS 15
#define PRODS 5

typedef struct{
	int id_h;
	int id_p;
	int x;
}produto;

int r;	// Para gerar valores aleatórios
produto vec[TOTALPRODS];
produto vec1[PRODS];
produto vec2[PRODS];
produto vec3[PRODS];

int count1=0;		// Contadores para indice do vetor de cada Hipermercado
int count2=0;
int count3=0;

pthread_mutex_t muxVec1;
pthread_mutex_t muxVec2;
pthread_mutex_t muxVec3;

int precoVec1;		// Preco para cada Hipermercado
int precoVec2;
int precoVec3;

void inicializaVetor(produto vec[]);
int gerador(int min,int max);
void printss(produto vecz[]);
void *thread_filtro();
void* calculaPrecoTotalHiper(void* arg);

int main(){
	pthread_t thread[3];
	int args[3];
	int i;

	inicializaVetor(vec);
	
	pthread_mutex_init(&muxVec1,NULL);
	pthread_mutex_init(&muxVec2,NULL);
	pthread_mutex_init(&muxVec3,NULL);

	/*for(i = 0; i < 15; i++){
		printf("ID Hipermercado:%d \n", vec[i].id_h);
		printf("ID Produto:%d \n", vec[i].id_p);
		printf("Preço Produto:%d \n\n", vec[i].x);
	}*/

	for(i=0;i<3;i++)	//Cria 3 threads
	{
		printf("Threads creating\n");
		pthread_create(&thread[i], NULL,thread_filtro, NULL);
	}
	for(i=0;i<3;i++)	// Espera pelos threads
		pthread_join(thread[i], NULL);
	
	printss(vec1);
	printss(vec2);
	printss(vec3);
	for(i = 0; i < 3; i++){		//Cria mais 3 threads
		args[i] = i;
		pthread_create(&thread[i], NULL, calculaPrecoTotalHiper, (void*)&args[i]);
	}
	for(i=0;i<3;i++)	// Espera pelos threads
		pthread_join(thread[i], NULL);
	
	int menorPreco = precoVec1;								// Determina menor
	if(menorPreco > precoVec2){ menorPreco = precoVec2;}
	if(menorPreco > precoVec3){ menorPreco = precoVec3;}
	
	printf("\nPreco Total:\nHipermercado 1: %.d\nHipermercado 2: %.d\nHipermercado 3: %.d\n", precoVec1, precoVec2, precoVec3);
	printf("\nPreco menor: %.d\n", menorPreco);
		
	return EXIT_SUCCESS;
}

void *thread_filtro(){
	int i;
	for(i = 0; i < 15; i++){
		if(vec[i].id_h == 1)
		{
			pthread_mutex_lock(&muxVec1);				// Bloqueia de forma a que a escrita no vetor seja segura
			int j;int bool = 1;
			for(j=0; j<5;j++){						
				if(vec1[j].id_p == vec[i].id_p){		// Verifica se o elemento de vec já existe em vec1
					bool = 0;
				}
			}
			if(bool == 1){
				vec1[count1] = vec[i];
				count1++;
			}
			pthread_mutex_unlock(&muxVec1);				// Desbloqueia, permitindo assim que os threads em espera prossigam.
		}
		if(vec[i].id_h == 2)
		{
			pthread_mutex_lock(&muxVec2);
			int j;int bool = 1;
			for(j=0; j<5;j++){
				if(vec2[j].id_p == vec[i].id_p){
					bool = 0;
				}
			}
			if(bool == 1){
				vec2[count2] = vec[i];
				count2++;
			}
			pthread_mutex_unlock(&muxVec2);
		}
		if(vec[i].id_h == 3)
		{
			pthread_mutex_lock(&muxVec3);
			int j;int bool = 1;
			for(j=0; j<5;j++){
				if(vec3[j].id_p == vec[i].id_p){
					bool = 0;
				}
			}
			if(bool == 1){
				vec3[count3] = vec[i];
				count3++;
			}
			pthread_mutex_unlock(&muxVec3);
		}
	}
	pthread_exit(NULL);
}


void* calculaPrecoTotalHiper(void* arg){
	int id = *((int*)arg);
	int i;
	switch(id){		// Filtra os HiperMercados
		case 0:		// Para cada vetor soma o preco de cada produto
			for(i = 0; i < 5; i++){
				precoVec1 += vec1[i].x;
			}
			break;
		case 1:
			for(i = 0; i < 5; i++){
				precoVec2 += vec2[i].x;
			}
			break;
		case 2:
			for(i = 0; i < 5; i++){
				precoVec3 += vec3[i].x;
			}
			break;
	}
	
	pthread_exit(NULL);
}

void inicializaVetor(produto vec[]){
	int i;
	int k = 1, r = 1;
	for(i = 0; i < 15; i++){
		if(i != 0 && i % 5 == 0) {
			r++;
			k = 1;
		}
		vec[i].id_h = r;
		vec[i].id_p = k;
		vec[i].x = gerador(1,20);
		k++;
	}
}

int gerador(int min,int max){
    srand((unsigned)time(NULL)+getpid()+getppid()*r);
    r++;
    return (rand() %max)+1;
}

void printss(produto vecz[]){
	int i;
	for(i = 0; i < 5; i++){
		printf("ID Hipermercado:%d \n", vecz[i].id_h);
		printf("ID Produto:%d \n", vecz[i].id_p);
		printf("Preço Produto:%d \n\n", vecz[i].x);
	}
}




