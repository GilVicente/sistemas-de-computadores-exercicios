#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h> /* Para constantes de “modo” */
#include <fcntl.h>

#define CHILDS 10
#define SIZE 500

typedef struct{
	char path[SIZE];
	char *word;
	int occurences;
	
} file_info;

int main(void){
	
	int fd;
	int data_size = sizeof(file_info) * 10;
	file_info *sh_file_info;
	int i;
	pid_t pid;
	int w_length; // tamanho da palavra a ser procurada
	int count = 0;	  // contador de palavras
	
	// For the file read
	FILE *fp;
    char buff[255];
    char *buffer;
	
			/* Criar o objeto de memória partilhada */
	fd = shm_open("/shm_ex06aa", O_CREAT|O_RDWR,S_IRUSR|S_IWUSR);
	/* Ajustar o tamanho da memória partilhada */
	ftruncate (fd, data_size);
	/* Mapear a memória partilhada */
	sh_file_info = (file_info*)  mmap(NULL,data_size,PROT_READ|PROT_WRITE,MAP_SHARED,fd,0);
	
	

	
	for(i = 0; i < CHILDS; i++){	// Fills the shared memory		
		sprintf(sh_file_info[i].path,"/media/partilha/scomp/scomp15162dbg09/modulo3/ex6/text%d",i+1);
		//printf("%s\n", sh_file_info[i].path);
		sh_file_info[i].word = "A";
		sh_file_info[i].occurences = 0;
		
	}

	for(i = 0; i < CHILDS; i++){		// Cria 10 filhos do pai
		pid = fork();
		if(pid == 0){
			
			fp = fopen(sh_file_info[i].path, "r");		// Reads file into buffer
			fgets(buff, 255, (FILE*)fp);
			//printf("%d: %s\n",i, buff);
			
			w_length = strlen(sh_file_info[i].word);	// Gets word length
			buffer = buff;
			
			//printf("%s\n\n",buffer);
			   do{														// This do-while counts the number of 'words'
					buffer = strstr(buffer, sh_file_info[i].word);
					//printf("haystack1: %s \n", buffer);
		
					if(buffer == NULL){
						break;
					}
					if(strlen(buffer) >= w_length){
						buffer = buffer+w_length;
					}		
					count++;
			  }while(buffer != NULL);
			
			//printf("Count: %d\n", count);
			sh_file_info[i].occurences = count;
			exit(0);
		}	
	}
	
	if(pid > 0){						// Para o pai
		for(i = 0; i < CHILDS; i++){		// Espera pelos 10 filhos
			wait(NULL);
		}
		
		for(i = 0; i < CHILDS; i++){
			printf("Number of occurences for each child: %d \n", sh_file_info[i].occurences);
		}
	}
	
	
	return 0;
	
}
