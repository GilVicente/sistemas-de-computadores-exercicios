#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h> /* Para constantes de “modo” */
#include <fcntl.h>
#include <time.h> 
 

int main(void){
	
	int fd;
	int *sh_v;
	int data_size = sizeof(int)*10;
	pid_t pid;
	int i,j;
	int max = 0;
	
	srand(time(NULL));
	int full_v[1000];
	
	// Randomize
	for(i=0; i<1000; i++){
		full_v[i] = (rand() % 1000);
	}


	/* Criar o objeto de memória partilhada */
	fd = shm_open("/shm_ex04", O_CREAT|O_RDWR,S_IRUSR|S_IWUSR);
	/* Ajustar o tamanho da memória partilhada */
	ftruncate (fd, data_size);
	/* Mapear a memória partilhada */
	sh_v = (int*) mmap(NULL,data_size,PROT_READ|PROT_WRITE,MAP_SHARED,fd,0);
	
	
	for(i=0;i<10;i++){ // Cria 10 processos filho
		pid = fork();
		
		if (pid == 0){							// Processo filho
			int partial_max = 0;
			
			for(j=i*100; j<i*100+100; j++){		// Percorre full vector - 100 posx de cada vez
				if(full_v[j] > partial_max){	// Determina máximo parcial
					partial_max = full_v[j];
				}	
			}
			sh_v[i] = partial_max;
			break;								//Sai do ciclo se for, evitando assim filhos do filho
		}

	}
	
	if (pid > 0){
		
		for(i = 0; i < 10; i++){				// Espera pelo fim dos 10 filhos
				wait(NULL);
		}
		
		for(i = 0; i < 10; i++){				// Determina o valor máximo
			if(sh_v[i] > max){
				max = sh_v[i];
			}
		}
		printf("Máximo == %d \n", max);
		
											//fechar memória partilhada
		// Desfaz mapeamento
		munmap(sh_v, data_size);
		// Fecha o descritor devolvido pelo shm_open
		close(fd);
		// O Leitor apaga a memória Partilhada do Sistema 
		shm_unlink("/shm_ex04"); 
		
	}
	
	
	return 0;
}
