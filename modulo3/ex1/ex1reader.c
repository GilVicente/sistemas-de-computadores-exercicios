#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h> /* Para constantes de “modo” */
#include <fcntl.h> 

typedef struct {
	int number;
	char name[20];
} student;

int main(int argc, char *argv[]) {
	int fd, data_size = sizeof(student);
	student *shared_student;
	fd = shm_open("/shmtest", O_EXCL|O_RDWR,S_IRUSR|S_IWUSR);
	ftruncate (fd, data_size);
	
	shared_student = (student*)mmap(NULL,data_size,PROT_READ|PROT_WRITE,MAP_SHARED,fd,0);
	printf("Reader - Number: %d\n", shared_student->number);
	printf("Reader - Name: %s\n", shared_student->name);
	
	//fechar memória partilhada
	munmap(shared_student, 100);
	close(fd);
	shm_unlink("/shmtest"); 
	
	return 0;
}
