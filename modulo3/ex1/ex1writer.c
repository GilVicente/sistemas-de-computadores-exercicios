#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h> /* Para constantes de “modo” */
#include <fcntl.h> 

#define size 20							//gcc -Wall -o shm_test shm_test.c -lrt

typedef struct {
	int number;
	char name[size];
} student;

int main(int argc, char *argv[]) {
	int fd;	/* Descritor da memória partilhada */

	int data_size = sizeof(student);	/* Tamanho da estrutura estudante */

	student *shared_student;	/* Apontador para a memória partilhada */

	/* Criar o objeto de memória partilhada */
	fd = shm_open("/shmtest", O_CREAT|O_EXCL|O_RDWR,S_IRUSR|S_IWUSR);

	/* Ajustar o tamanho da memória partilhada */
	ftruncate (fd, data_size);

	/* Mapear a memória partilhada */
	shared_student = (student*)mmap(NULL,data_size,PROT_READ|PROT_WRITE,MAP_SHARED,fd,0);

	printf("Writer - Number:");
	scanf("%d,",&shared_student->number);
	
	printf("Writer - Name: ");
	scanf("%s", shared_student->name);
	
	return 0;
}

