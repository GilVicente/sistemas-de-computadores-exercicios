#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h> /* Para constantes de “modo” */
#include <fcntl.h> 

#define size 20

typedef struct {
	int number;
	char name[size];
} student;

int main(int argc, char *argv[]) {
	int fd, data_size = sizeof(student);
	student *shared_student;
	
	fd = shm_open("/shmtest", O_CREAT|O_EXCL|O_RDWR,
	S_IRUSR|S_IWUSR);
	ftruncate (fd, data_size);
	shared_student = (student*)mmap(NULL,data_size,PROT_READ|PROT_WRITE,MAP_SHARED,fd,0);
	shared_student->number = 1;
	shared_student->name = "melklkl";
	
	return 0;
}

