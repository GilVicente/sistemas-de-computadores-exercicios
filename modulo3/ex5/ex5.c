#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h> /* Para constantes de “modo” */
#include <fcntl.h>


#define STR_SIZE 50
#define NR_DISC 10

typedef struct{
	int numero;
	char nome[STR_SIZE];
	int disciplinas[NR_DISC];
} aluno;

int main(void){
	
	int fd;
	aluno *sh_aluno;
	int data_size = sizeof(aluno);
	pid_t pid;
	int i;
	
		/* Criar o objeto de memória partilhada */
	fd = shm_open("/shm_ex05", O_CREAT|O_RDWR,S_IRUSR|S_IWUSR);
	/* Ajustar o tamanho da memória partilhada */
	ftruncate (fd, data_size);
	/* Mapear a memória partilhada */
	sh_aluno = (aluno*) mmap(NULL,data_size,PROT_READ|PROT_WRITE,MAP_SHARED,fd,0);
	
	sh_aluno->disciplinas[9] = -1; // Prepara espera activa
	
	pid = fork();
	
	if( pid > 0){
		printf("Numero: ");					// O user insere a informação
		scanf("%d", &sh_aluno->numero);
		printf("Nome: ");
		scanf("%s", sh_aluno->nome);
	
		for(i = 0; i < NR_DISC; i++){
			printf("Disciplina[%d] :", i);
			scanf("%d", &sh_aluno->disciplinas[i]);
		}
		
		wait(NULL);						// Espera pelo fim do filho
		
				// Desfaz mapeamento
		munmap(sh_aluno, data_size);
		// Fecha o descritor devolvido pelo shm_open
		close(fd);
		// O Leitor apaga a memória Partilhada do Sistema 
		shm_unlink("/shm_ex05"); 
	}
	
	if (pid == 0){
		int min = 0, max = 0;
		int sum = 0;
		while(sh_aluno->disciplinas[9] == -1);		// Espera activa pelo preenchimento da nota da última disciplica 
		
		for (i = 0; i < NR_DISC; i++){
			if(sh_aluno->disciplinas[i] < min){
				min = sh_aluno->disciplinas[i];
			}
			if(sh_aluno->disciplinas[i] > max){
				max = sh_aluno->disciplinas[i];
			}
			sum += sh_aluno->disciplinas[i];
		}
		
		printf("Highest grade: %d\n",min);
		printf("Lowest grade: %d\n",max); 
		printf("Average grade: %d\n",sum/NR_DISC);  
	}
	
	return 0;
	
}
