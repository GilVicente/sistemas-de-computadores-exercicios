#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h> /* Para constantes de “modo” */
#include <fcntl.h> 

#define operations 100000000

typedef struct {
    int value;
} sh_data;

int main(int argc, char *argv[]) {
	int fd;	/* Descritor da memória partilhada */
	int data_size = sizeof(int);
	sh_data *sh;

	/* Criar o objeto de memória partilhada */
	fd = shm_open("/shm_ex03", O_CREAT|O_EXCL|O_RDWR,S_IRUSR|S_IWUSR);

	/* Ajustar o tamanho da memória partilhada */
	ftruncate (fd, data_size);

	/* Mapear a memória partilhada */
	sh = (sh_data*) mmap(NULL,data_size,PROT_READ|PROT_WRITE,MAP_SHARED,fd,0);
	
	sh->value = 100;
	
	// to be continued..
	
	// Cria processo filho
	
	pid_t pid;
	int i;
	pid = fork();
	
	if (pid == 0){		
		for(i=0; i<operations; i++){
			sh->value = sh->value+1;
			
			sh->value = sh->value-1;

		}
		
		printf("Filho Value: %d \n", sh->value);
		
	}
	
	
	if ( pid > 0){		
		for(i=0; i<operations; i++){
			sh->value = sh->value+1;

			
			sh->value = sh->value-1;

		}
		printf("Pai Value: %d \n", sh->value);
	}
	
									//fechar memória partilhada
	// Desfaz mapeamento
	munmap(sh, data_size);
	// Fecha o descritor devolvido pelo shm_open
	close(fd);
	// O Leitor apaga a memória Partilhada do Sistema 
	shm_unlink("/shm_ex03"); 
	
	return 0;

}
