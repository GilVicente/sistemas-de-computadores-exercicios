#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h> /* Para constantes de “modo” */
#include <fcntl.h>

int main(void){
	
	int fd;
	int *sh_v;
	int data_size = 10 * sizeof(int);
	int i, average, sum = 0;
	
	// Abrir o objeto da memória partilhada 
	fd = shm_open("/shm_ex02", O_CREAT | O_RDWR, S_IRWXU | S_IRWXG);
	
	// Ajustar o tamanho da memória partilhada
	ftruncate (fd, data_size);
	// Mapear a memória partilhada
	sh_v = (int *) mmap(NULL, data_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
	
	for(i=0; i<10; i++){
		sum += sh_v[i];
	}
	
	average = sum/10;
	
	printf("Average: %d\n", average);
	
								//fechar memória partilhada
	// Desfaz mapeamento
	munmap(sh_v, data_size);
	// Fecha o descritor devolvido pelo shm_open
	close(fd);
	// O Leitor apaga a memória Partilhada do Sistema 
	shm_unlink("/shm_ex02"); 
	
	return 0;
}
