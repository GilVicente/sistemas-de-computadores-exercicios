#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h> /* Para constantes de “modo” */
#include <fcntl.h>
#include <time.h> 

int main(void){
	
	int fd;
	int *sh_v;
	int data_size = 10 * sizeof(int);
	
	fd = shm_open("/shm_ex02", O_CREAT | O_RDWR, S_IRWXU | S_IRWXG);
	/* Ajustar o tamanho da memória partilhada */
	ftruncate (fd, data_size);
	sh_v = (int *) mmap(NULL, data_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);


	   /* -- Escrever na memória partilhada -- */
	srand(time(NULL)); /* Inicializar Gerador Números Aleatórios */
	int i;
    // Gera 10 valores aleatórios (entre 1 e 20) e adiciona-os no vector
	for (i = 0; i < 10; ++i) {
		sh_v[i] = (rand() % 20) + 1;
	}
	
	return 0;
}
