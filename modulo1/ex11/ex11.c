#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>


char cria_gemeos(pid_t lista[2]) {
	int i;										// Função cria gémeos
	pid_t pid;
	for (i = 0; i < 2; ++i)
	{
		pid = fork();
		if (pid > 0) {
			lista[i] = pid;
		}
		if (lista[i] == -1) {
			perror("Erro!");
			exit(EXIT_FAILURE);
		} else if(i == 0 && pid == 0) {
			// 1º filho
			return 'a';
		} else if(i == 1 && pid == 0) {
			// 2º filho
			return 'b';
		}
	}

	return 'p';
}

int main(void){
	pid_t lista[6];
	int i;
	char retorno;
	int parent;
	parent = getpid();
	for(i=0;i < 3; i++){							// Alinea a)
		retorno = cria_gemeos(lista+(i*2));
		
		if(retorno == 'a'){
			printf("Filho: %d \n",lista[(1*i)-1]);
			printf("Pai: %d \n", parent);
		}
		
		if(retorno == 'b'){
			printf("Filho: %d \n",lista[(2*i)-1]);
			printf("Pai: %d \n", parent);
		}
	}
	
	
	
}
