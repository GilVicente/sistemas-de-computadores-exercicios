#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>

void filho1(){
	printf("Eu sou o primeiro filho com o PID - %d\n", getpid());
	sleep(5);
	exit(1);
}

void filho2(){
	printf("Eu sou o segundo filho com o PID - %d\n", getpid());
	exit(2);
}

int main(void){
	pid_t p1, p2;
	p1 = fork();
	if(p1 > 0 ){
		printf("Eu sou o pai com o PID  %d\n", getpid());
		p2 = fork();
		if(p2 == -1) {
			perror("Fork falhou no 2º\n"); exit(-1);
		} else if(p2 == 0){
			filho2();
		}
		int status = 0;
		waitpid(p1, &status, 0);
		if(WIFEXITED(status)) printf("O filho %d retornou o valor:%d\n", p1, WEXITSTATUS(status));
		else exit(-1);
		waitpid(p2, &status, 0);
		if(WIFEXITED(status)) printf("O filho %d retornou o valor:%d\n", p2, WEXITSTATUS(status));
		else exit(-1);
	} else if (p1 == -1) {
		perror("Fork falhou no 1º\n"); exit(-1);
	}
	else {
		filho1();
	}
		
	return 0;
}
