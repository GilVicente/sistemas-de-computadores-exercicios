#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

int main(void){
	pid_t p, a;
	p=fork();
	if(p == -1){
		printf("Erro: processo nao criado");
	}
	a=fork();
	if(a == -1){
		printf("Erro: processo nao criado");
	}
	printf("Sistemas de Computadores\n");
	return 0;
	
}
