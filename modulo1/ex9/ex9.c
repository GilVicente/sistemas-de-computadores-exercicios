#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>
#define val 200000

int main(void){
	//pid_t parentID = getpid();
	//printf("PID: %d\n",parentID);
	pid_t pid;
	int start = 0;
	int i;
	for(i=0; i < 6; i++){
		pid = fork();
		if(pid == 0){
			start = val * i;
			break; // Apenas o pai tem filhos
		}
	}

	
	for(i = start; i < start + val; i++){
			printf("%d\n", i+1);
	}
	
	return 0;
}
