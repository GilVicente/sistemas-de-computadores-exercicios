#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char **argv) {
    int i = 1;
    pid_t pid;

    while (i != argc) {
        pid = fork();
        if (pid == -1) {
            perror("Erro!");
            exit(EXIT_FAILURE);
        } else if (pid == 0) {
            // Processe filho executa a funcao
            int retorno;

            retorno = execlp(argv[i], argv[i], NULL);
            exit(retorno); 
        }
        i++; // Vai para o próximo comando
    }

    if (pid > 0)
    {
        int status;						// O pai espera
        for (i = 1; i < argc; ++i)
        {
            wait(&status);
            if (!WIFEXITED(status)) {
                perror("Erro!");
                exit(EXIT_FAILURE);                
            }
        }
    }

    return EXIT_SUCCESS;
}
