#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>

#define val 50000


int main(void) {
	pid_t pid;
	int i;
	
	
	int* dados = (int*) calloc(val*2, sizeof(int));
	for (i = 0; i < val*2; ++i) {
		dados[i] = i;
	}

	
	int* resultado = (int*) malloc(sizeof(int) * val*2);
	pid = fork();	

	if (pid == 0) {
		

		for (i = 0; i < val; ++i) {
			resultado[i] = dados[i]*4 + 20;
			printf("%d\n", resultado[i]);	
		}

	} else if (pid > 0)	{
		
		for (i = val; i < val*2; ++i) {
			resultado[i] = dados[i]*4 + 20;
		}
		
		int status;
		wait(&status);

		if (WIFEXITED(status)) {
			for (i = 0; i < val*2; ++i) {
				printf("%d\n", resultado[i]);
			}
				
		} 
		}
	return 0;
}
