#include <stdio.h> 
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>

int main(void){
	pid_t pid[3];
	int f;

	for(f=0;f<3;f++){
		pid [f]= fork (); 
		
		if (pid[f] == -1) {
			printf("Fork Falhou!\n");
			exit(1);
		} else if (pid[f]== 0) { 
			sleep(1);
			exit(f+1);
		}
	}
	
	if (pid > 0) { 
		printf("Eu sou o PAI\n");
			int status;

		waitpid(pid[1], &status, WNOHANG); 
	
		if (WIFEXITED(status)) {
			printf("Processo-Filho Nr. %d - PID %d\n", WEXITSTATUS(status), pid[1]);
		} else {
			printf("O processo-filho com o PID %d não terminou normalmente!\n", pid[1]);
		}
	}
	return 0;
}
