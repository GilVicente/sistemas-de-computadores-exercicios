#include <stdio.h> 
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>

int main(void){
	pid_t pid;
	int f;

	for(f=0;f<3;f++){
		pid = fork (); 
		
		if (pid == -1) {
			printf("Fork Falhou!\n");
			exit(1);
		} else if (pid == 0) { 
			sleep(1);
			exit(0);
		} else { 
			printf("Eu sou o PAI\n");
		}
	}
	return 0;
}
