// exercico 4 a)
// sao criados 7 processos = 2^ -1 
// b)
// sao criados 7 processos

#include <stdio.h> 

#include <unistd.h>

 #include <sys/types.h>

int main(void){
	pid_t pid;
	int f;

	for(f=0;f<3;f++){
		pid = fork (); 
		if (pid > 0){

		printf("Eu sou o PAI\n");   
	}
	else{
		sleep(1);
		}

	}
	return 0;
}
