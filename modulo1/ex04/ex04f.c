#include <stdio.h> 
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>

int main(void){
	pid_t pid;
	int f;

	for(f=0;f<3;f++){
		pid = fork (); 
		
		if (pid == -1) {
			printf("Fork Falhou!\n");
			exit(1);
		} else if (pid == 0) { 
			sleep(1);
			exit(0);
		}
	}
	
	if (pid > 0) { 
		printf("Eu sou o PAI\n");
		int status, i;
		pid_t pf;

		for (i = 0; i < 3; i++) {
			pf = wait(&status); 
			if (!WIFEXITED(status)) { 
				printf("O processo-filho com o PID %d não terminou normalmente!\n", pf);
			}
		}
	}

	return 0;
}
