#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#define size 100000

int main(void) {
	pid_t pid, i;
	int* vec = (int *) malloc(size*sizeof(int));
	
	
	for (i = 0; i < size; ++i)
	{
		vec[i] = i;
	}

	int search = 23313;

	
	for (i = 0; i < 5; ++i)
	{
		pid = fork();
		if (pid == 0)
		{
			int lim = 20000*(i+1);
			int j;
			for (j = (lim-20000)+1; j < lim; ++j)
			{
				if (vec[j] == search)
				{
					exit(i+1);
				}
			}
			exit(0);
		} else if (pid == -1){
			printf("ERRO: O fork falhou. O processo vai terminar...\n");
		}
	}

	if (pid > 0) {
		int status;
		int pos;
		
		for (i = 0; i < 5; ++i){
			wait(&status);
			if(WIFEXITED(status)) { 
				pos = WEXITSTATUS(status);
				if (pos >=0 && pos <= 5 ) {
					if (pos > 0){
						printf("O número %d foi encontrado no processo-filho nr. %d.\n", search, pos);
					}
				}
			}
		}
	}	
	return 0;
}
