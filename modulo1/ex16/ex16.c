#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>


void error(){
	printf("ERRO"); exit(-1);
	}


void funct(char* opt){
	execlp(opt, opt, (char*)0);
	printf("ERRO NO COMANDO!\n");
	exit(-1);
}

int main(void){
	printf("======= INCIANDO A SHELL ======\n");
	int status = 0;
	while(1){
		char opt[100];
		printf("~$");
		fgets(opt, 100, stdin);
		  int i = strlen(opt)-1;
		  if( opt[ i ] == '\n') 
			  opt[i] = '\0';
		if(!strcmp(opt, "sair")) break;
		printf("\n");
		pid_t p = fork();
		if(p == 0){
			funct(opt);
		}
		else waitpid(p, &status, 0);
		
	}
	return 0;
}
