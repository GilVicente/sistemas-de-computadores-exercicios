#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>

void M(char* c){
	printf("%s",c);
}

int main(void) {
	pid_t pid;
	int i;

	for (i = 0; i < 2; ++i) {
		pid = fork();

		if (pid > 0) {
		    M("A");
			wait(); // Espera pelo filho
		}else if (pid == 0) {
			M("B");
			pid = fork(); // Cria novo filho 

			// Caso seja o filho, agora pai, que criou um novo filho
			if (pid > 0) {
				M("A");
				break;	// Termina
			} 
		}
	}

	return 0;
}
